package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedlist;

public class CarList {
	private static CircularSortedDoublyLinkedlist<Car> list = new CircularSortedDoublyLinkedlist<Car>(new CarComparator());
	static {}
	private CarList() {}
	public static CircularSortedDoublyLinkedlist<Car> getinstance() {
		return list;
	}
	public static void resetCars() {
		list.clear();
	}
}
