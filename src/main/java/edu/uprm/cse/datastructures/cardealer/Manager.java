package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedlist;

@Path("/cars")
public class Manager {
	private final CircularSortedDoublyLinkedlist<Car> list = CarList.getinstance();

//	corre por la lista y la devuele como un array de carros 
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		int i = 0;
		Car[] newlist = new Car[list.size()];
		for (Car car : list) {
			newlist[i] = car;
			i++;
		}
		return newlist;
	}
	
// corre la lista buscando un carro con un id en especifico y lo devuelve, de no encontrarlo devuelve un exception
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getCarId() == id) {
				return list.get(i);
			}
		}
		throw new NotFoundException("Car" + id + "not found");

	}
	
// corre la lista y verifica si el id del carro no esta en la misma y lo agrega, de ya existir el id en la lista, no te deja agregarlo
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCustomer(Car cars) {
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getCarId() == cars.getCarId()) {
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}
		list.add(cars);
		return Response.status(201).build();

	}

// corre la lista y verifica si el id esta en la misma, de ser asi, update el ya existente en el sistema. De no encontrarlo te indica que el mismo no fue encontrado
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update (Car cars, @PathParam("id") int id) {
		for (Car car : list) {
			if(car.getCarId() == id) {
				list.remove(car);
				list.add(cars);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}

//	corre la lista y verifica si el id del carro esta en la misma y de ser asi lo elimina. De no encontrarlo el programa tirar un exception
	@DELETE
	@Path("/{id}/delete")
	public Response delete (@PathParam("id")long id) {
		for (Car car : list) {
			if(car.getCarId() == id) {
				list.remove(car);
				return Response.status(200).build();
			}
		}
		throw new NotFoundException("Car" + id + "not found");

	}
}
