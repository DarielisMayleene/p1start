package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car o1, Car o2) {
		String CarBrand1 = o1.getCarBrand();
		String CarBrand2 = o2.getCarBrand();
		String CarModel1 = o1.getCarModel();
		String CarModel2 = o2.getCarModel();
		String CarModelOption1 = o1.getCarModelOption();
		String CarModelOption2 = o2.getCarModelOption();
		
		if(CarBrand1.equals(CarBrand2)) {
			if(CarModel1.equals(CarModel2)) {
				if(CarModelOption1.equals(CarModelOption2)) {
//	verifica si los carros son iguales tanto en brand como en model y model option
					return 0;
				}
// si en model option no son iguales, los sortea y organiza comparando el model option de uno con el del otro
				return CarModelOption1.compareTo(CarModelOption2);
			}
// si solamente son iguales en el brand.. los sortea y organiza comparando el model del 1 con el model del otro
			return CarModel1.compareTo(CarModel2);
		}
// los sortea y organiza con el brand y esto significa que no son iguales en ninguna de las tres, ni brand, ni model y model option 
		return CarBrand1.compareTo(CarBrand2);
	}
}
