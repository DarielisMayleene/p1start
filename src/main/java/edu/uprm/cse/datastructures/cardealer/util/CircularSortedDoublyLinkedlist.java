package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedlist<E> implements SortedList<E> {
	private Node<E> header;
	private Comparator<E> comparator;
	private int currentSize;

	public  CircularSortedDoublyLinkedlist(Comparator<E> comparator) {
		this.header = new Node<>();
		this.comparator = comparator;
		this.currentSize = 0;
	}

	private class CircularDoublyLinkedlistIterator<E> implements Iterator<E>{
		private Node<E> nextNode;


		public CircularDoublyLinkedlistIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}
	@Override
	public Iterator<E> iterator() {
		return new CircularDoublyLinkedlistIterator<E> ();
	}

	@Override
	
	public boolean add(E obj) {
		if(this.isEmpty()) {
			Node<E> temp = new Node<E>(obj,header,header);
			this.header.setNext(temp);
			this.header.setPrev(temp);
			this.currentSize++;
			return true;
		}

		Node<E> nuevoNode = new Node<E>(obj, null, null);
		Node<E> Node = header;
// se comparan con el comparator creado para poder organizarlos 
		E o1 = Node.getElement();
		E o2 = nuevoNode.getElement();

		for(Node=this.header.getNext(); Node!=header; Node= Node.getNext()) {
			if(comparator.compare(Node.getElement(), o2)>0) {
				nuevoNode.setNext(Node);
				nuevoNode.setPrev(Node.getPrev());
				Node.getPrev().setNext(nuevoNode);
				Node.setPrev(nuevoNode);
				currentSize++;
				return true;
			}
		}

		nuevoNode.setPrev(header.getPrev());;
		nuevoNode.setNext(header);
		header.setPrev(nuevoNode);
		nuevoNode.getPrev().setNext(nuevoNode);
		currentSize++;
		return true;

	}

	public int size() {
		return this.currentSize;
	}

	@Override
//	elimina un elemento en especifico y nos deja saber si lo elimino o no y ajusta el circular doubly linked list
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if(i < 0) {
			return false;
		} else {
			this.remove(i);
			return true;
		}

	}

	@Override
//	elimina el elemento en un index especifico y ajusta el circular doubly linked list y nos deja saber si lo elimino o no
	public boolean remove(int index) {
		if( index < 0 || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}
		if(this.isEmpty()) {
			return false;
		}
		Node<E> temp = this.header;
		Node<E> target = this.header;
		int i = 0;
		while(i != index) {
			temp = temp.getNext();
			i++;
		}
		target = temp.getNext();
		temp.setNext(target.getNext());
		target.getNext().setPrev(temp);
		target.setNext(null);
		target.setElement(null);
		this.currentSize--;
		return true;
	}


@Override
// corre por el circular doubly linked list eliminando todos los elementos iguales a obj y nos devuelve cuantas veces los elimino 
public int removeAll(E obj) {
	int result = 0; 
	while(this.remove(obj)) {
		result++;
	}
	return result;
}

@Override
//devuelve el primer elemento en el circular doubly linked list 
public E first() {
	if(this.isEmpty()) {
		return header.getElement();
	}
	return header.getNext().getElement();
}

@Override
//devuelve el ultimo elemento en el circular doubly linked list 
public E last() {
	if(this.isEmpty()) {
		return header.getElement();
	}
	return header.getPrev().getElement();
}


@Override
public E get(int index) {
	if(index < 0 || index >= this.currentSize) {
		throw new IndexOutOfBoundsException();
	}
	int i = 0;
	Node<E> temp = this.header.getNext();
	while(i != index) {
		temp = temp.getNext();
		i++;
	}
	return temp.getElement();
}

@Override
//elimina todo hasta dejar el circular doubly linked list vacio
public void clear() {
	while(!this.isEmpty()) {
		this.remove(0);
	}

}

@Override
public boolean contains(E e) {
	if(firstIndex(e)> 0) {
		return true;
	}
	return false;
}

@Override
public boolean isEmpty() {
//	nos indica si el circular doubly linked list  no contiene nada
	return this.size() == 0;
}

@Override
//corre por el circular doubly linked list hasta que encuentra por primera vez el index en donde esta e; de llegar de nuevo al header significa que no lo encontro y devuelve -1
public int firstIndex(E e) {
	int i = 0;
	int result = -1;
	for (Node<E> temp = this.header.getNext(); temp != header; 
			temp = temp.getNext(), ++i) {
		if(temp.getElement().equals(e)) {
			return result = i;
		}
	}
	return result;
}

@Override
//corre por el circular doubly linked list hasta que encuentra el ultimo index en donde esta e; de llegar de nuevo al header significa que no lo encontro y devuelve -1
public int lastIndex(E e) {
	int i = 0;
	int result = -1;
	for (Node<E> temp = this.header.getNext(); temp != header; 
			temp = temp.getNext(), ++i) {
		if(temp.getElement().equals(e)) {
			 result = i;
		}
	}

	return result;
}

}
